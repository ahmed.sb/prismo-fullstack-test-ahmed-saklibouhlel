const Koa = require('koa');
const Router = require('koa-router');
const logger = require('koa-morgan');
const server = new Koa();
const router = new Router();
const bodyParser = require('koa-body')() ;
const cors = require('@koa/cors');
server.use(cors()) ;
var knex = require('knex')({
    client: 'mysql',
    connection: {
      host : '127.0.0.1',
      user : 'root',
      password : 'prismo',
      database : 'prismo_nodejs_test'
    }
  });

router.get('/members' ,(ctx,next) => {
    return knex('member').then(function(data){ 
        ctx.body=data ;
        next();
    })
    
    }) ;
    
router.get('/member/:id',(ctx,next) => {
    return knex('member').where({id: ctx.params.id.slice(1)}).then(function(data){ 
        ctx.body=data ;
        next() ;
    })
    }) ;

router.post('/member',bodyParser,async ctx => {
    let mid= await knex('member').max('id as maxId').first()
    const newMember={
        id: (mid.maxId+1),
        firstname:ctx.request.body.firstname,
        lastname:ctx.request.body.lastname,
        avatar:ctx.request.body.avatar+".png",
        function:ctx.request.body.function,
        description:ctx.request.body.description
        
    }
    knex('member').insert(newMember).then(res=>{
    ctx.body='success'
    })
    
  }) ;

router.get('/welcome' ,(ctx,next) => {
    
        ctx.body='bonjour !' ;
        
    })
    server
    .use(logger('tiny'))
    .use(router.routes())
    .listen(3001)
  