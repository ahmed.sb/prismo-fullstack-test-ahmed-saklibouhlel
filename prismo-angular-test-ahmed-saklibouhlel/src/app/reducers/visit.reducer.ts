import { Action } from '@ngrx/store'
import { Visit } from './../models/visit'
import * as VisitActions from './../actions/visit.actions'
const initialState : Visit ={
    firstname : '',
    lastname : '',
    avatar : '',
    function : '',
    description : '',
    date : new Date()
}
export function reducer(state: Visit[] = [initialState], action: VisitActions.Actions) {

    switch(action.type) {
        case VisitActions.ADD_VISIT:
            return [...state, action.payload];
        default:
            return state;
    } 
}