import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component'
import { MembersListComponent } from './members-list/members-list.component';
import { MemberComponent } from './member/member.component'
import { CreateMemberComponent } from './create-member/create-member.component';
import { HistoricComponent } from './historic/historic.component';

const routes: Routes = [
  {path: '', component : WelcomeComponent},
  {path: 'welcome', component : WelcomeComponent},
  {path: 'members-list', component : MembersListComponent},
  {path: 'member/:id', component : MemberComponent},
  {path: 'create-member', component : CreateMemberComponent},
  {path: 'historic', component : HistoricComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
