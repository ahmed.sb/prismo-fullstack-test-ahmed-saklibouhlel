import { Component, OnInit } from '@angular/core';
import { Member } from './../models/member.model'
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-create-member',
  templateUrl: './create-member.component.html',
  styleUrls: ['./create-member.component.scss']
})
export class CreateMemberComponent implements OnInit {

  constructor(private httpService: HttpClient, private router : Router) { }
  member={
    firstname:"",
    lastname:"",
    avatar:"",
    function:"",
    description:""
  }
  ngOnInit(): void {
  }

  addMember() {
    console.log(this.member)
    this.httpService.post("http://localhost:3001/member",this.member).subscribe((val)=>{
      
    },err=>{
      console.log(err)
    })
    this.router.navigate(['/members-list']) ;
    
  }
}
