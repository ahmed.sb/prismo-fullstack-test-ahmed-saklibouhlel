import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { Member } from '../models/member.model'
import { Store } from '@ngrx/store';
import { AppState } from './../app.state';
import { Visit } from './../models/visit'
import * as VisitActions from './../actions/visit.actions';
import { Observable , BehaviorSubject } from 'rxjs/';
@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss']
})
export class MemberComponent implements OnInit {

  constructor(private httpService: HttpClient ,route: ActivatedRoute ,private store: Store<AppState>) {
    this.id = route.snapshot.params.id;

  }
  member: Member ;
  id : string ;

  addVisit(m) {
    this.store.dispatch(new VisitActions.AddVisit({firstname: m.firstname, lastname: m.lastname, avatar: m.avatar, function :m.func, description : m.description, date: new Date()}) )
    console.log(this.store)
  }
  ngOnInit(): void {
    this.httpService.get('http://localhost:3001/member/'+this.id).subscribe(
      data => {
        this.member=data[0]; 
        this.addVisit(this.member) 
        this.store.select(state => state).subscribe(data => { 
          localStorage.setItem('historic',JSON.stringify(data.visit))
        });   
        
      },
      (errs: HttpErrorResponse) => {
      console.log(errs.error)
      }
    );

  }

}
