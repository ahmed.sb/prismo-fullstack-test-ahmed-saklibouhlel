import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Member } from '../models/member.model';
import { trigger, transition, style, animate, query, stagger } from '@angular/animations';


@Component({
  selector: 'app-members-list',
  templateUrl: './members-list.component.html',
  animations: [
    trigger('listAnimation', [
      transition('* => *', [ 
        
        query(':enter', [
          style({ opacity: 0 }),
          stagger(100, [
            animate('0.2s', style({ opacity: 1 }))
          ])
        ], { optional: true })
      ])
    ])
  ],
  styleUrls: ['./members-list.component.scss']
})
export class MembersListComponent implements OnInit {

  constructor (private httpService: HttpClient) { }
  arrMembers: Member [];

  ngOnInit(): void {
     this.httpService.get('http://localhost:3001/members').subscribe(
      data => {
      
        this.arrMembers = data as Member [];	 
     
       
      },
      (err: HttpErrorResponse) => {
        console.log(err.error) ;
      }
    );
  }
  logAnimation(_event) {
    console.log(_event)
  }

}
