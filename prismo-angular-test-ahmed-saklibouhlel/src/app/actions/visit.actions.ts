import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { Visit } from '../models/visit'

    export const ADD_VISIT = '[VISIT] Add'
    export const REMOVE_VISIT = '[VISIT] Remove'
    export class AddVisit implements Action {
        readonly type = ADD_VISIT
        
        constructor(public payload: Visit) {}
    }
    export class RemoveVisit implements Action {
        readonly type = REMOVE_VISIT
    
        constructor(public payload: number) {}
    }
export type Actions = AddVisit | RemoveVisit;