export interface Visit {
    firstname : string ;
    lastname : string ;
    avatar : string ;
    function : string ;
    description : string ;
    date : Date ;
}