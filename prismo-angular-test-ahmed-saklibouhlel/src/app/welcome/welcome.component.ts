import { Component, OnInit } from '@angular/core' ;
import { HttpClient } from '@angular/common/http' ;
import { Router } from '@angular/router';

@Component({ templateUrl : 'welcome.component.html',styleUrls: ['./welcome.component.scss']}) 
export class WelcomeComponent implements OnInit{
    page ={
        title: 'Bienvenue',
        subtitle: 'Bienvenue chez Prismo !',
        content: 'Some home content.',
        image: 'https://prismo.io/wp-content/uploads/2019/10/prismo_thumb.png'
    } ;
    constructor(){}
    ngOnInit(){}

   
}