import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-historic',
  templateUrl: './historic.component.html',
  styleUrls: ['./historic.component.scss']
})
export class HistoricComponent implements OnInit {

  constructor() { }
  visits : [] ;

  ngOnInit(): void {
    
    this.visits=JSON.parse(localStorage.getItem('historic')) ;
    this.visits.shift() ;
    console.log(this.visits)

  }
 

}
